package poc.graphql.infra;

import atumus.core.application.interfaces.AtumusContext;
import atumus.ignite.cache.interfaces.NodeClientCacheQuery;
import com.paygo.schema.merchant.domain.Merchant;
import com.paygo.schema.merchant.interfaces.MerchantCacheName;
import graphql.schema.DataFetchingEnvironment;
import io.vertx.core.Future;
import lombok.RequiredArgsConstructor;
import org.apache.ignite.Ignite;
import org.apache.ignite.binary.BinaryObject;
import org.apache.ignite.lang.IgniteBiPredicate;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class MerchantGraphQLQuery {

    private static final MerchantCacheName CACHE_NAME = new MerchantCacheName();

    private final AtumusContext context;
    private final Ignite igniteClient;

    public void findAll(final DataFetchingEnvironment dataFetchingEnvironment, final Future<List<Merchant>> future) {
        final NodeClientCacheQuery<String, Merchant> cacheQuery = new NodeClientCacheQuery<>(context, igniteClient, CACHE_NAME);

        // TODO fazer método findAll sem parâmetro
        final IgniteBiPredicate<String, BinaryObject> predicate = (key, binaryObject) -> true;

        cacheQuery.findAll(predicate, result -> {
            final List<Merchant> merchants = result.stream()
                    .map(entry -> (Merchant) entry.getValue().deserialize())
                    .collect(Collectors.toList());

            final String cpfCnpj = dataFetchingEnvironment.getArgument("cpfCnpj");
            if(cpfCnpj != null) {
                future.complete(merchants.stream()
                        .filter(merchant -> merchant.getCpfCnpj().equals(cpfCnpj))
                        .collect(Collectors.toList()));
                return;
            }

            future.complete(merchants);
        }, error -> context.getLogger().error("Query error", error));
    }
}
