package poc.graphql.infra;

import atumus.core.application.interfaces.AtumusContext;
import atumus.ignite.cache.interfaces.NodeClientCacheQuery;
import com.paygo.schemas.schedule.domain.ScheduleView;
import com.paygo.schemas.schedule.interfaces.ScheduleCacheName;
import graphql.schema.DataFetchingEnvironment;
import io.vertx.core.Future;
import lombok.RequiredArgsConstructor;
import org.apache.ignite.Ignite;
import org.apache.ignite.binary.BinaryObject;
import org.apache.ignite.lang.IgniteBiPredicate;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ScheduleGraphQLQuery {

    private static final ScheduleCacheName CACHE_NAME = new ScheduleCacheName();

    private final AtumusContext context;
    private final Ignite igniteClient;

    public void findAll(final DataFetchingEnvironment dataFetchingEnvironment, final Future<List<ScheduleView>> future) {
        final NodeClientCacheQuery<Long, ScheduleView> cacheQuery = new NodeClientCacheQuery<>(context, igniteClient, CACHE_NAME);

        final String authorizationUUID = dataFetchingEnvironment.getArgument("authorizationUUID");
        if(authorizationUUID != null) {
            findByAuthorizationUUID(future, cacheQuery, authorizationUUID);
            return;
        }

        // TODO fazer método findAll sem parâmetro
        final IgniteBiPredicate<Long, BinaryObject> predicate = (key, binaryObject) -> true;
        cacheQuery.findAll(predicate, result -> {
            final List<ScheduleView> schedules = result.stream()
                    .map(entry -> (ScheduleView) entry.getValue().deserialize())
                    .collect(Collectors.toList());


            future.complete(schedules);
        }, error -> {
            context.getLogger().error("Query error", error);
            future.fail(error);
        });
    }

    private void findByAuthorizationUUID(final Future<List<ScheduleView>> future, final NodeClientCacheQuery<Long, ScheduleView> cacheQuery, final String authorizationUUID) {
        final IgniteBiPredicate<Long, BinaryObject> predicate = (key, binaryObject) -> {
            final BinaryObject scheduleObject = binaryObject.field("schedule");
            return scheduleObject.field("authorizationUUID").equals(authorizationUUID);
        };

        cacheQuery.findOne(predicate, result -> {
            final ScheduleView scheduleView = result.getValue().deserialize();
            future.complete(Collections.singletonList(scheduleView));
        }, error -> {
            context.getLogger().error("Query error", error);
            future.fail(error);
        });
    }
}
