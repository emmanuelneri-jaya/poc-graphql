package poc.graphql.infra;

import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.GraphQLError;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.Handler;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static io.netty.handler.codec.http.HttpResponseStatus.BAD_REQUEST;
import static io.netty.handler.codec.http.HttpResponseStatus.OK;

public final class GraphQLRouterHandler {

    public Handler<RoutingContext> createRoutingContextHandler(final GraphQL graphQL) {
        return handler -> {
            handler.request().bodyHandler(bodyHandler -> {
                final JsonObject queryJson = new JsonObject(bodyHandler.toString());
                final String query = queryJson.getString("query");

                final ExecutionInput executionInput = new ExecutionInput(query, null, queryJson, null, extractVariables(queryJson));
                final ExecutionResult executionResult = graphQL.execute(executionInput);

                final JsonObject response = new JsonObject();
                if (executionResult.getData() != null) {
                    final Map<String, Object> data = executionResult.getData();
                    response.put("data", new JsonObject(Json.encode(data)));
                }

                final List<GraphQLError> errors = executionResult.getErrors();
                if (errors != null && !errors.isEmpty()) {
                    final JsonArray jsonErrors = parseErrors(errors);
                    response.put("errors", jsonErrors);
                }

                final HttpResponseStatus statusCode = (executionResult.getErrors() != null && !errors.isEmpty()) ? BAD_REQUEST : OK;

                handler.response().putHeader("Content-Type", "application/json");
                handler.response().setStatusCode(statusCode.code());
                handler.response().end(response.toString());
            });
        };
    }

    private JsonArray parseErrors(final List<GraphQLError> errors) {
        final JsonArray jsonErrors = new JsonArray();
        errors.forEach(error -> {
            final JsonObject jsonError = new JsonObject();
            jsonError.put("message", error.getMessage());
            jsonErrors.add(jsonError);
        });
        return jsonErrors;
    }

    private static Map<String, Object> extractVariables(final JsonObject request) {
        final JsonObject variables = request.getJsonObject("variables");
        return Objects.isNull(variables) ? Collections.emptyMap() : variables.getMap();
    }

}
