package poc.graphql.application;

import atumus.core.application.interfaces.AtumusApplication;
import atumus.core.application.interfaces.Module;
import atumus.ignite.core.interfaces.IgniteClientNodeStarter;


public class GraphQLApplication {

	public static void main(String[] args) {
		final Module module = Module.valueOf("poc-graphql");

		AtumusApplication.create(args, module).start(context -> {

			IgniteClientNodeStarter.create(context).start(igniteClient -> {
				context.deploy(new GraphQLVerticle(context, igniteClient));
			});
		});
	}
}
