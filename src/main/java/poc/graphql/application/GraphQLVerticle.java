package poc.graphql.application;

import atumus.core.application.interfaces.AtumusContext;
import atumus.core.application.interfaces.AtumusVerticle;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.graphql.VertxDataFetcher;
import lombok.RequiredArgsConstructor;
import org.apache.ignite.Ignite;
import poc.graphql.infra.GraphQLRouterHandler;
import poc.graphql.infra.MerchantGraphQLQuery;
import poc.graphql.infra.ScheduleGraphQLQuery;

import static graphql.schema.idl.RuntimeWiring.newRuntimeWiring;

@RequiredArgsConstructor
public class GraphQLVerticle extends AtumusVerticle {

    private final static int HTTP_SERVER_PORT = 8080;
    private final AtumusContext context;
    private final Ignite igniteClient;

    @Override
    public void start(final Future<Void> startFuture) {

        final HttpServer httpServer = context.getVertx().createHttpServer();
        final Router router = Router.router(context.getVertx());

        createGraphQL(graphQL -> {
            final GraphQLRouterHandler graphQLRouterHandler = new GraphQLRouterHandler();
            router.get("/graphql").handler(graphQLRouterHandler.createRoutingContextHandler(graphQL));
        });

        final Future<Void> future = Future.future();
        startServer(httpServer, router, future);
    }

    private void createGraphQL(final Handler<GraphQL> graphQLHandler) {
        context.getVertx()
                .fileSystem()
                .readFile("schema.graphqls", handler -> {
                    if(handler.failed()) {
                        this.context.getLogger().error("fail to read schema.graphqls");
                        return;
                    }

                    final String schema = handler.result().toString();
                    final TypeDefinitionRegistry typeDefinitionRegistry = new SchemaParser().parse(schema);

                    final MerchantGraphQLQuery merchantGraphQLQuery = new MerchantGraphQLQuery(context, igniteClient);
                    final ScheduleGraphQLQuery scheduleGraphQLQuery = new ScheduleGraphQLQuery(context, igniteClient);

                    final RuntimeWiring runtimeWiring = newRuntimeWiring()
                            .type("Query", builder -> builder
                                    .dataFetcher("allMerchants", new VertxDataFetcher<>(merchantGraphQLQuery::findAll))
                                    .dataFetcher("allSchedules", new VertxDataFetcher<>(scheduleGraphQLQuery::findAll)))
                            .build();

                    final SchemaGenerator schemaGenerator = new SchemaGenerator();
                    final GraphQLSchema graphQLSchema = schemaGenerator
                            .makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);


                    graphQLHandler.handle(GraphQL.newGraphQL(graphQLSchema).build());
                });
    }

    private static void startServer(final HttpServer httpServer, final Router router, final Future<Void> future) {
        httpServer.requestHandler(router)
                .listen(HTTP_SERVER_PORT, listenHandler -> {
                    if(listenHandler.failed()) {
                        future.fail(listenHandler.cause());
                    }

                    future.complete();
                });
    }
}
