# POC GraphQL com Vert.x e Atumus

###Executando projeto

- Execute o starter do atumus para iniciar o Ignite
- Execute a classe main ``poc.graphql.application.GraphQLApplication``

###exemplos

```
[GET] http://localhost:8080/graphql
query {
    allMerchants {
        name,
        cpfCnpj
    }    
}    

```
Resultado: 
```
{
    "data": {
        "allMerchants": [
            {
                "name": "Merchant 1",
                "cpfCnpj": "1111111111111"
            },
            {
                "name": "Merchant 2",
                "cpfCnpj": "2222222222222"
            },
            {
                "name": "Merchant 3",
                "cpfCnpj": "33333333333333"
            },
            {
                "name": "Merchant 4",
                "cpfCnpj": "44444444444444"
            }
        ]
    }
}
```

```
[GET] http://localhost:8080/graphql
query {
    allMerchants(cpfCnpj: "13.375.900/0001-65") {
        name,
        cpfCnpj
    }    
}   

```
Resultado: 
```
{
    "data": {
        "allMerchants": [
            {
                "name": "Merchant 1",
                "cpfCnpj": "1111111111111"
            }
        ]
    }
}
```

```
[GET] http://localhost:8080/graphql
query {
    allSchedules {
        id,
        schedule {
            id, 
            authorizationUUID
        }
    }    
}      
```
Resultado: 
```
{
    "data": {
        "allSchedules": [
            {
                "id": 123,
                "schedule": {
                    "id": 123,
                    "authorizationUUID": "123"
                }
            }
        ]
    }
}
```

